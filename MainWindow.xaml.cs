﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace TestConnectionProjectCs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void incomingTest(IPAddress adress, Int32 port)
        {
            try
            {
                TcpListener listener = new TcpListener(adress, port);
                listener.Start();
                TcpClient client = listener.AcceptTcpClient();
                Dispatcher.BeginInvoke(new Action(delegate()
                {
                    FromPeerRadioButton.IsChecked = true;
                }));
                client.Close();
                listener.Stop();
            }
            catch (Exception E)
            {
                MessageBox.Show(E.Message);
            }
        }
        private void CheckConnectionButton_Click(object sender, RoutedEventArgs e)
        {
            ToPeerRadioButton.IsChecked = false;
            FromPeerRadioButton.IsChecked = false;

            IPAddress IpAdress = IPAddress.Parse(IpTextBox.Text);
            Int32 Port = Convert.ToInt32(PortTextBox.Text);
            new Thread(() => incomingTest(IpAdress, Port)).Start();
            try
            {
                //new TcpClient(IpTextBox.Text, Port);
                ToPeerRadioButton.IsChecked = true;
            }
            catch (Exception E)
            {
                MessageBox.Show(E.Message);
            }
        }
    }
}
